#!/bin/bash
CMAKE_OPTIONS="-Wno-dev -DCMAKE_TOOLCHAIN_FILE=$PS2SDK/ps2dev.cmake -DCMAKE_INSTALL_PREFIX=$PS2SDK/ports -DBUILD_SHARED_LIBS=OFF "
#CMAKE_OPTIONS+="-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON "

function build {
    mkdir $1
    cd $1
    cmake $CMAKE_OPTIONS $3 ../../$2 || { exit 1; }
    make clean all || { exit 1; }
    make install || { exit 1; }
    cd ..
}

./checkenv.sh || { exit 1; }

## NOTE: Building all libraries will also wipe the $PS2SDK folder
rm -rf $PS2SDK
rm -rf $GSKIT

cd libs

##
## Build ps2sdk first
##
cd ps2sdk
make clean all install || { exit 1; }
cd ..

## gcc needs to include both libc and libkernel from ps2sdk to be able to build executables.
## NOTE: There are TWO libc libraries, gcc needs to include them both.
ln -sf "$PS2SDK/ee/lib/libc.a"      "$PS2DEV/ee/ee/lib/libps2sdkc.a" || { exit 1; }
ln -sf "$PS2SDK/ee/lib/libkernel.a" "$PS2DEV/ee/ee/lib/libkernel.a" || { exit 1; }

## Add ps2dev.cmake
cp ../ps2dev.cmake $PS2SDK/ || { exit 1; }

##
## Build cmake projects
##
rm -rf build
mkdir build
cd build
build zlib      ps2sdk-ports/zlib/src
build libpng    ps2sdk-ports/libpng/src "-DPNG_SHARED=OFF -DPNG_STATIC=ON"
build freetype  ps2sdk-ports/freetype2/src
build libconfig libconfig "-DBUILD_EXAMPLES=OFF -DBUILD_TESTS=OFF"
cd ..

##
## Fix legacy applications using libz.a instead of libzlib.a
##
ln -sf "$PS2SDK/ports/lib/libzlib.a" "$PS2SDK/ports/lib/libz.a"

##
## Build required ps2sdk-ports libraries
##
cd ps2sdk-ports
make libjpeg  || { exit 1; }
make libtiff  || { exit 1; }
make stlport  || { exit 1; }
make ucl      || { exit 1; }
cd ..

##
## Build others
##
cd ps2eth && make clean all install && cd .. || { exit 1; }
cd isjpcm && make clean all install && cd .. || { exit 1; }

##
## Build gsKit
##
cd gsKit
export LIBJPEG=$PS2SDK/ports
export LIBPNG=$PS2SDK/ports
export ZLIB=$PS2SDK/ports
#export LIBTIFF=$PS2SDK/ports
make clean all install || { exit 1; }
cd ..

##
## Build SDL from ps2sdk-ports last
## becouse it depends on gsKit and more
##
cd ps2sdk-ports
make sdl      || { exit 1; }
make sdlgfx   || { exit 1; }
make sdlimage || { exit 1; }
make sdlmixer || { exit 1; }
make sdlttf   || { exit 1; }
cd ..

cd ..
