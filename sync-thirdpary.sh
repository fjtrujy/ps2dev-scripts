#!/bin/bash

function rebase-upstream {
	remoterepo=$1
	remotebranch=$2

	echo "Syncing $3 to $remoterepo/$remotebranch"

	# Fetch the latest upstream commits
	git fetch $remoterepo $remotebranch --quiet

	# Get the latest commit hash
	sha="$(git rev-parse $remoterepo/$remotebranch)"
	#echo "sha=$sha"

	# Are we on the master-ps2max branch?
	if [ "$(git rev-parse --abbrev-ref HEAD)" != "master-ps2max" ]; then
		echo "- Not on master-ps2max branch!"
		return 1
	fi

	# Do we need to update?
	if [ -z "`git rev-list HEAD..$sha --`" ]; then
		echo "- Branch already up to date"
		sha="$(git rev-parse HEAD)"
		if [ -z "`git rev-list ps2max/master-ps2max..$sha --`" ]; then
			echo "- Branch already pushed"
		else
			echo "- Branch needs push!"
			echo "- Push"
			git push --force || { return 1; }
		fi
		return 0
	fi

	# Can we update?
	git update-index -q --refresh
	if ! git diff-index --quiet HEAD --; then
		echo "- Branch is dirty"
		return 1
	fi

	echo "- Rebasing..."
	git pull || { return 1; }
	git rebase $remoterepo/$remotebranch || { return 1; }

	echo "- Push"
	git push --force || { return 1; }
}

cd tools/ps2-packer           && rebase-upstream ps2dev master ps2-packer      || { exit 1; }
cd ../../libs/gsKit           && rebase-upstream ps2dev master gsKit           || { exit 1; }
cd ../../libs/ps2sdk          && rebase-upstream ps2dev master ps2sdk          || { exit 1; }
cd ../../libs/ps2eth          && rebase-upstream ps2dev master ps2eth          || { exit 1; }
cd ../../libs/isjpcm          && rebase-upstream AKuHAK master isjpcm          || { exit 1; }
cd ../../libs/ps2sdk-ports    && rebase-upstream ps2dev master ps2sdk-ports    || { exit 1; }
cd ../../apps/open-ps2-loader && rebase-upstream ifcaro master open-ps2-loader || { exit 1; }
cd ../../apps/uLaunchELF      && rebase-upstream AKuHAK master uLaunchELF      || { exit 1; }
cd ../../apps/ps2link         && rebase-upstream ps2dev master ps2link         || { exit 1; }
