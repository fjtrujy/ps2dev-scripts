#!/bin/bash

./checkenv.sh || { exit 1; }

## NOTE: Building a new toolchain will also wipe the $PS2DEV folder
rm -rf $PS2DEV/*
rm -rf toolchain/build

./build-binutils.sh   ee  || { exit 1; }
./build-binutils.sh   iop || { exit 1; }
./build-binutils.sh   dvp || { exit 1; }
./build-gcc-stage1.sh ee  || { exit 1; }
./build-gcc-stage1.sh iop || { exit 1; }
./build-newlib.sh     ee  || { exit 1; }
./build-gcc-stage2.sh ee  || { exit 1; }
