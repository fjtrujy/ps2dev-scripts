FROM alpine:latest

ENV PS2DEV_ROOT /usr/local/ps2dev
ENV PS2DEV      $PS2DEV_ROOT
ENV PS2SDK      $PS2DEV/ps2sdk
ENV GSKIT       $PS2DEV/gsKit
ENV PATH        $PATH:${PS2DEV}/bin
ENV PATH        $PATH:${PS2DEV}/ee/bin
ENV PATH        $PATH:${PS2DEV}/iop/bin
ENV PATH        $PATH:${PS2DEV}/dvp/bin
ENV PATH        $PATH:${PS2SDK}/bin

# repo will also checkout ps2dv-scripts
# so we don't need to copy the source directory
#COPY . /src

RUN \
  apk add --no-cache make bash && \
  apk add --no-cache --virtual .build-deps build-base git curl python perl gettext bison flex cmake zlib-dev ucl-dev && \
  \
  mkdir ~/bin && \
  PATH=~/bin:$PATH && \
  curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo && \
  chmod a+x ~/bin/repo && \
  \
  mkdir src && \
  cd /src && \
  repo init -u https://gitlab.com/ps2max/ps2dev-repo -b ps2max --depth=1 && \
  repo sync -j4 -c --no-tags && \
  ./build-toolchain-git.sh && \
  ./build-libs.sh && \
  ./build-tools.sh && \
  \
  apk del .build-deps && \
  rm -rf \
    /src/* \
    /tmp/*

WORKDIR /src
