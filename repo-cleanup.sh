#!/bin/bash

# Restore zlib
cd libs/ps2sdk-ports/zlib/src/
git checkout -- zconf.h
rm -f zconf.h.included
cd ../../../..

# Remove binutils generated files that are missing in the .gitignore
rm -f toolchain/dvp/binutils/binutils/po/zh_CN.gmo
rm -f toolchain/dvp/binutils/opcodes/po/ro.gmo
rm -f toolchain/ee/binutils/binutils/po/zh_CN.gmo
rm -f toolchain/ee/binutils/opcodes/po/ro.gmo
rm -f toolchain/iop/binutils/binutils/po/zh_CN.gmo
rm -f toolchain/iop/binutils/opcodes/po/ro.gmo
