#!/bin/bash

./checkenv.sh || { exit 1; }

TARGET=$1
TARG_XTRA_OPTS=""

## Determine the maximum number of processes that Make can work with.
OSVER=$(uname)
if [ ${OSVER:0:10} == MINGW32_NT ]; then
	PROC_NR=$NUMBER_OF_PROCESSORS
elif [ ${OSVER:0:6} == Darwin ]; then
	PROC_NR=$(sysctl -n hw.ncpu)
else
	PROC_NR=$(nproc)
fi

## Create and enter the toolchain/build directory
mkdir -p toolchain/build && cd toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf binutils-$TARGET && mkdir binutils-$TARGET && cd binutils-$TARGET || { exit 1; }

## Configure the build.
../../$TARGET/binutils/configure --prefix="$PS2DEV/$TARGET" --target="$TARGET" $TARG_XTRA_OPTS || { exit 1; }

## Compile and install.
make clean && make -j $PROC_NR CFLAGS="$CFLAGS -D_FORTIFY_SOURCE=0" && make install && make clean || { exit 1; }

## Exit the build directory.
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }
