#!/bin/bash

function rebase-upstream {
	echo "Syncing $1 to master-ps2max"

	# Get the latest commit hash
	sha="$(git rev-parse master-ps2max)"
	#echo "sha=$sha"

	# Do we need to update?
	if [ -z "`git rev-list HEAD..$sha --`" ]; then
		echo "- Branch already up to date"
		return 0
	fi

	# Can we update?
	git update-index -q --refresh
	if ! git diff-index --quiet HEAD --; then
		echo "- Branch is dirty"
		return 1
	fi

	echo "- Rebasing..."
	git rebase master-ps2max || { return 1; }
}

cd tools/ps2-packer           && rebase-upstream ps2-packer      || { exit 1; }
cd ../../libs/gsKit           && rebase-upstream gsKit           || { exit 1; }
cd ../../libs/ps2sdk          && rebase-upstream ps2sdk          || { exit 1; }
cd ../../libs/ps2eth          && rebase-upstream ps2eth          || { exit 1; }
cd ../../libs/isjpcm          && rebase-upstream isjpcm          || { exit 1; }
cd ../../libs/ps2sdk-ports    && rebase-upstream ps2sdk-ports    || { exit 1; }
cd ../../apps/open-ps2-loader && rebase-upstream open-ps2-loader || { exit 1; }
cd ../../apps/uLaunchELF      && rebase-upstream uLaunchELF      || { exit 1; }
cd ../../apps/ps2link         && rebase-upstream ps2link         || { exit 1; }
