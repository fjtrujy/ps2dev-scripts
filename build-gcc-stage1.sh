#!/bin/bash

./checkenv.sh || { exit 1; }

TARGET=$1

OSVER=$(uname)
## Apple needs to pretend to be linux
if [ ${OSVER:0:6} == Darwin ]; then
	TARG_XTRA_OPTS="--build=i386-linux-gnu --host=i386-linux-gnu"
else
	TARG_XTRA_OPTS=""
fi

## Determine the maximum number of processes that Make can work with.
if [ ${OSVER:0:10} == MINGW32_NT ]; then
	PROC_NR=$NUMBER_OF_PROCESSORS
elif [ ${OSVER:0:6} == Darwin ]; then
	PROC_NR=$(sysctl -n hw.ncpu)
else
	PROC_NR=$(nproc)
fi

## Prevent regeneration of c-parse.c since it contains a bug
touch toolchain/$TARGET/gcc/gcc/c-parse.c

## Create and enter the toolchain/build directory
mkdir -p toolchain/build && cd toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf gcc-$TARGET-stage1 && mkdir gcc-$TARGET-stage1 && cd gcc-$TARGET-stage1 || { exit 1; }

## Configure the build.
../../$TARGET/gcc/configure --prefix="$PS2DEV/$TARGET" --target="$TARGET" --enable-languages="c" --with-newlib --without-headers $TARG_XTRA_OPTS || { exit 1; }

## Compile and install.
make clean && make -j $PROC_NR && make install && make clean || { exit 1; }

## Exit the build directory
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }
